variable "region" {
  # Defines the data type of this variable as a string.
  type = string
  # Sets the default value for the region variable.
  default = "us-east-1"
}
